<?php namespace Jd\PrivatePlugins;

use Config;
use Backend;
use System\Classes\PluginBase;
use Jd\PrivatePlugins\Models\Settings;

/**
 * PrivatePlugins Plugin Information File
 */
class Plugin extends PluginBase
{

    public function __construct($app)
    {
        parent::__construct($app);

        $app->before(function() {
            $pluginSettings = Settings::instance();

            if($pluginSettings->enabled && $pluginSettings->address)
            {
                Config::set('cms.updateServer', $pluginSettings->address);
            }
        });
    }

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Private Plugins',
            'description' => 'Provide a private plugin server for your October CMS website',
            'author'      => 'JD',
            'icon'        => 'icon-compass'
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label' => 'Private Plugins',
                'description' => 'Manage Private Plugin server settings',
                'category' => 'system::lang.system.categories.system',
                'icon' => 'icon-cloud',
                'class' => 'Jd\PrivatePlugins\Models\Settings',
                'keywords' => 'private plugin plugins server',
                'permissions' => ['jd.privateplugins.settings'],
            ],
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'jd.privateplugins.settings' => [
                'tab' => 'Private Plugins',
                'label' => 'Manage settings'
            ],
        ];
    }

}
