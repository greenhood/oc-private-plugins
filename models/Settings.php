<?php namespace Jd\PrivatePlugins\Models;

/**
 * Setting Model
 */
use Model;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'jd_privateplugins_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}